//
//  NSDictionary+Additions.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

// returns nil instead on NSNull object
// use this method instead of valueForKey or objectForKey
- (id)safeObjectForKey:(NSString *)key;
- (id)safeObjectForKeyPath:(NSString *)keyPath;

@end
