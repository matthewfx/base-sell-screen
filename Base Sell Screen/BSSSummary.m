//
//  BSSSummary.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 26/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSSummary.h"
#import "BSSCoreDataHelper.h"
#import "LineItem+Addition.h"

@implementation BSSSummary

- (instancetype)initWithDiscountPercentage:(NSString *)discountPercentageString;
{
    self = [super init];
    
    if (self) {
        
        NSArray *allItems = [[BSSCoreDataHelper sharedInstance] objectsWithEntity:@"LineItem"
                                                                           sortBy:nil
                                                                  predicateFormat:nil];
        if ([discountPercentageString isEqualToString:@""]) {
            self.discountPercentage = 0.0;
        } else if ([discountPercentageString doubleValue] > 100.0) {
            self.discountPercentage = 100.0;
        } else {
            self.discountPercentage = [discountPercentageString doubleValue];
        }
        
        for (LineItem *item in allItems) {
            double allProductsPrice = [item.productPrice doubleValue] * [item.productQuantity doubleValue];
            double currentLineItemWithDiscount = allProductsPrice - (allProductsPrice/100.0 * self.discountPercentage);
            double taxForCurrentLineItem = currentLineItemWithDiscount * [item.productTaxRate doubleValue];
            double totalForTheCurentLineItem = currentLineItemWithDiscount + taxForCurrentLineItem;
            self.discount += currentLineItemWithDiscount;
            self.subtotal += allProductsPrice ;
            self.tax += taxForCurrentLineItem;
            self.total += totalForTheCurentLineItem;
        }

    }
    
    return self;
}

- (instancetype)init
{
    self = [self initWithDiscountPercentage:@""];
    return self;
}

@end
