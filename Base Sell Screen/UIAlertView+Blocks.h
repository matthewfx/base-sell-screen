//
//  UIAlertView+Blocks.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 26/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CompletionBlockWithUserEnteredText)(NSString *userText);
typedef void (^CancelBlock)();

@interface UIAlertView (Blocks)

+ (UIAlertView *)showAlertViewWithTitle:(NSString *)title
                                message:(NSString *)message
                           cancelButton:(NSString *)cancelButtonTitle
                      otherButtonTitle:(NSString *)otherButtonTitle
                              onDismiss:(CompletionBlockWithUserEnteredText)completed
                               onCancel:(CancelBlock)cancelled;

@end
