//
//  BSSSummaryTableViewController.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSSummaryTableViewController.h"
#import "BSSSummaryTableViewCell.h"

static NSString *SummaryCellIdentifier = @"SummaryCell";

@interface BSSSummaryTableViewController ()

@property (nonatomic, strong) BSSSummary *summary;

@end

@implementation BSSSummaryTableViewController


- (void)setupTableView
{
    [super setupTableView];
    

    [self.tableView registerClass:[BSSSummaryTableViewCell class]
              forCellReuseIdentifier:SummaryCellIdentifier];
    [self refreshTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshTable)
                                                 name:@"ProductChanged"
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"ProductChanged"
                                                  object:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    BSSSummaryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SummaryCellIdentifier
                                                                    forIndexPath:indexPath];
    
    self.summary = [[BSSSummary alloc] initWithDiscountPercentage:self.percentageDiscountString];

    switch (indexPath.row) {
        case 0:
            cell.cellNameLabel.text = @"Sub-total";
            cell.priceLabel.text = [NSString stringWithFormat:@"%.2f",self.summary.subtotal];
            break;
        
        case 1:
            cell.cellNameLabel.text = [NSString stringWithFormat:@"After %.2f %% discount",self.summary.discountPercentage];
            cell.priceLabel.text = [NSString stringWithFormat:@"%.2f",self.summary.discount];
            break;
            
        case 2:
            cell.cellNameLabel.text = @"Tax (GST)";
            cell.priceLabel.text = [NSString stringWithFormat:@"%.2f",self.summary.tax];
            break;

            
        case 3:
        {
            UIFont *cellFont = [UIFont fontWithName:@"Lato-Bold" size:14.0];
            cell.cellNameLabel.text = @"Total Price";
            cell.priceLabel.text = [NSString stringWithFormat:@"%.2f",self.summary.total];
            cell.cellNameLabel.font = cellFont;
            cell.priceLabel.font = cellFont;
            break;
        }
    }
    
    [cell setNeedsUpdateConstraints];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (void)setPercentageDiscountString:(NSString *)percentageDiscountString
{
    _percentageDiscountString = percentageDiscountString;
    [self refreshTable];
}


- (void)refreshTable
{
    [self.tableView reloadData];
}

@end
