//
//  BSSSummary.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 26/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSSSummary : NSObject

@property (nonatomic, assign) double subtotal;
@property (nonatomic, assign) double tax;
@property (nonatomic, assign) double discount;
@property (nonatomic, assign) double discountPercentage;
@property (nonatomic, assign) double total;

- (instancetype)initWithDiscountPercentage:(NSString *)discountPercentageString;


@end
