//
//  LineItem+Addition.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "LineItem+Addition.h"
#import "BSSCoreDataHelper.h"
#import "NSDictionary+Additions.h"

@implementation LineItem (Addition)

+ (LineItem *)lineItemWithParams:(NSDictionary *)params
{
    LineItem *item = [[BSSCoreDataHelper sharedInstance] createObjectWithEntity:@"LineItem"];
    
    item.productQuantity    = [params safeObjectForKey:@"quantity"];
    item.productName        = [params safeObjectForKey:@"name"];
    item.productPrice       = [params safeObjectForKey:@"price"];
    item.productTaxRate     = [params safeObjectForKey:@"tax"];
    item.productAdded       = [params safeObjectForKey:@"date"];
    
    return item;
}

- (void)updateLineItemProductQuantityWithUserText:(NSString *)userText
{
    if ([userText isEqualToString:@""]) {
        return;
    } else if ([userText integerValue] == 0) {
        [[BSSCoreDataHelper sharedInstance] deleteObject:self];
    } else {
        self.productQuantity = @([userText integerValue]);
    }
}

- (void)updateLineItemProductPriceWithUserText:(NSString *)userText
{
    if ([userText isEqualToString:@""]) {
        return;
    } else {
        self.productPrice = @([userText doubleValue]);
    }
}

@end
