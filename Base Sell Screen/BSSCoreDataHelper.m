//
//  BSSCoreDataHelper.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSCoreDataHelper.h"

@implementation BSSCoreDataHelper

#pragma mark - FILES
NSString *storeFilename = @"BaseSellScreen.sqlite";
NSString *sourceStoreFilename = @"DefaultData.sqlite";

#pragma mark - PATHS
- (NSString *)applicationDocumentsDirectory
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class,NSStringFromSelector(_cmd));
    }
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}
- (NSURL *)applicationStoresDirectory
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    NSURL *storesDirectory =
    [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]]
     URLByAppendingPathComponent:@"Stores"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory
                  withIntermediateDirectories:YES
                                   attributes:nil
                                        error:&error]) {
            if (debug==1) {
                NSLog(@"Successfully created Stores directory");}
        }
        else {NSLog(@"FAILED to create Stores directory: %@", error);}
    }
    return storesDirectory;
}
- (NSURL *)storeURL
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    return [[self applicationStoresDirectory]
            URLByAppendingPathComponent:storeFilename];
}
- (NSURL *)sourceStoreURL
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    
    return [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                   pathForResource:[sourceStoreFilename stringByDeletingPathExtension]
                                   ofType:[sourceStoreFilename pathExtension]]];
}


#pragma mark - SETUP
+ (id)sharedInstance
{
    static BSSCoreDataHelper *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSSCoreDataHelper alloc] init];
    });
    return sharedInstance;
}


- (id)init
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    self = [super init];
    
    return self;
}

- (void)loadCoreData
{
    _model = [NSManagedObjectModel mergedModelFromBundles:nil];
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
    
    _parentContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_parentContext performBlockAndWait:^{
        [_parentContext setPersistentStoreCoordinator:_coordinator];
        [_parentContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
    }];
    
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_context setParentContext:_parentContext];
    [_context setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
}


- (void)loadStore
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if (_store) {return;} // Don’t load store if it’s already loaded
    
    NSDictionary *options =
    @{
      NSMigratePersistentStoresAutomaticallyOption:@YES
      ,NSInferMappingModelAutomaticallyOption:@YES
      ,NSSQLitePragmasOption: @{@"journal_mode": @"DELETE"} // Uncomment to disable WAL journal mode
      };
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                        configuration:nil
                                                  URL:[self storeURL]
                                              options:options
                                                error:&error];
    if (!_store) {
        NSLog(@"Failed to add store. Error: %@", error);abort();
    }
    else         {NSLog(@"Successfully added store: %@", _store);}
    
}

- (void)setupCoreData
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    [self loadCoreData];
    [self loadStore];
    
}


#pragma mark - SAVING
- (void)saveContext
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    if ([_context hasChanges]) {
        NSError *error = nil;
        if ([_context save:&error]) {
            NSLog(@"_context SAVED changes to persistent store");
        } else {
            NSLog(@"Failed to save _context: %@", error);
            [self showValidationError:error];
        }
    } else {
        NSLog(@"SKIPPED _context save, there are no changes!");
    }
}
- (void)backgroundSaveContext
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // First, save the child context in the foreground (fast, all in memory)
    [self saveContext];
    
    // Then, save the parent context.
    [_parentContext performBlock:^{
        if ([_parentContext hasChanges]) {
            NSError *error = nil;
            if ([_parentContext save:&error]) {
                NSLog(@"_parentContext SAVED changes to persistent store");
            }
            else {
                NSLog(@"_parentContext FAILED to save: %@", error);
                [self showValidationError:error];
            }
        }
        else {
            NSLog(@"_parentContext SKIPPED saving as there are no changes");
        }
    }];
}

- (void)clearCoreData
{
    
    NSError *error;
    NSURL *fileUrl = [self storeURL];
    [[NSFileManager defaultManager] removeItemAtURL:fileUrl
                                              error:&error];
    
    _context = nil;
    _parentContext = nil;
    _model = nil;
    _coordinator = nil;
    _store = nil;
    
    [self setupCoreData];
}


#pragma mark - VALIDATION ERROR HANDLING
- (void)showValidationError:(NSError *)anError
{
    
    if (anError && [anError.domain isEqualToString:@"NSCocoaErrorDomain"]) {
        NSArray *errors = nil;  // holds all errors
        NSString *txt = @""; // the error message text of the alert
        
        // Populate array with error(s)
        if (anError.code == NSValidationMultipleErrorsError) {
            errors = [anError.userInfo objectForKey:NSDetailedErrorsKey];
        } else {
            errors = [NSArray arrayWithObject:anError];
        }
        // Display the error(s)
        if (errors && errors.count > 0) {
            // Build error message text based on errors
            for (NSError * error in errors) {
                NSString *entity =
                [[[error.userInfo objectForKey:@"NSValidationErrorObject"]entity]name];
                
                NSString *property =
                [error.userInfo objectForKey:@"NSValidationErrorKey"];
                
                switch (error.code) {
                    case NSValidationRelationshipDeniedDeleteError:
                        txt = [txt stringByAppendingFormat:
                               @"%@ delete was denied because there are associated %@\n(Error Code %li)\n\n", entity, property, (long)error.code];
                        break;
                    case NSValidationRelationshipLacksMinimumCountError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' relationship count is too small (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationRelationshipExceedsMaximumCountError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' relationship count is too large (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationMissingMandatoryPropertyError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' property is missing (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationNumberTooSmallError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' number is too small (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationNumberTooLargeError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' number is too large (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationDateTooSoonError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is too soon (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationDateTooLateError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is too late (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationInvalidDateError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' date is invalid (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringTooLongError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text is too long (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringTooShortError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text is too short (Code %li).", property, (long)error.code];
                        break;
                    case NSValidationStringPatternMatchingError:
                        txt = [txt stringByAppendingFormat:
                               @"the '%@' text doesn't match the specified pattern (Code %li).", property, (long)error.code];
                        break;
                    case NSManagedObjectValidationError:
                        txt = [txt stringByAppendingFormat:
                               @"generated validation error (Code %li)", (long)error.code];
                        break;
                        
                    default:
                        txt = [txt stringByAppendingFormat:
                               @"Unhandled error code %li in showValidationError method", (long)error.code];
                        break;
                }
            }
            // display error message txt message
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Validation Error"
//             
//                                       message:[NSString stringWithFormat:@"%@Please double-tap the home button and close this application by swiping the application screenshot upwards",txt]
//                                      delegate:nil
//                             cancelButtonTitle:nil
//                             otherButtonTitles:nil];
//            [alertView show];
        }
    }
}

#pragma mark – UNDERLYING DATA CHANGE NOTIFICATION
- (void)somethingChanged
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    // Send a notification that tells observing interfaces to refresh their data
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"SomethingChanged" object:nil];
}


#pragma mark - DELETING
- (void)deleteAllObjectsForEntityName:(NSString *)entity
{
    if (debug==1) {
        NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    }
    NSFetchRequest *fetchAllObjects = [[NSFetchRequest alloc] init];
    [fetchAllObjects setEntity:[NSEntityDescription entityForName:entity inManagedObjectContext:_context]];
    
    NSError *error = nil;
    NSArray *allObjects = [_context executeFetchRequest:fetchAllObjects error:&error];
    
    if (error) {
        NSLog(@"Error deleting %@",error.localizedDescription);
    }
    
    for (NSManagedObject *object in allObjects) {
        [_context deleteObject:object];
    }
    
//    [self backgroundSaveContext];
}


#pragma mark - Object creation / deletion

- (id)createObjectWithEntity:(NSString *)entityName
{
    id object = [NSEntityDescription
                 insertNewObjectForEntityForName:entityName
                 inManagedObjectContext:self.context];
    
    return object;
}

- (id)temporaryObjectWithEntity:(NSString *)entityName
{
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:entityName
                                   inManagedObjectContext:self.context];
    id object = [[NSManagedObject alloc]
                 initWithEntity:entity
                 insertIntoManagedObjectContext:nil];
    
    return object;
}

- (void)deleteObject:(NSManagedObject *)object
{
    if (object == nil)
        return;
    
    [self.context deleteObject:object];
}

- (id)duplicateObject:(NSManagedObject *)source temporary:(BOOL)temporary
{
    NSManagedObjectContext *duplicated;
    if (temporary)
        duplicated = [self temporaryObjectWithEntity:source.entity.name];
    else
        duplicated = [self createObjectWithEntity:source.entity.name];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [[NSEntityDescription
                                 entityForName:source.entity.name
                                 inManagedObjectContext:self.context] attributesByName];
    
    for (NSString *attr in attributes) {
        [duplicated setValue:[source valueForKey:attr] forKey:attr];
    }
    
    //Loop through all relationships, and clone them.
    NSDictionary *relationships = [[NSEntityDescription
                                    entityForName:source.entity.name
                                    inManagedObjectContext:self.context] relationshipsByName];
    for (NSRelationshipDescription *rel in relationships){
        NSString *keyName = [NSString stringWithFormat:@"%@",rel];
        
        //get a set of all objects in the relationship
        NSMutableSet *sourceSet = [source mutableSetValueForKey:keyName];
        NSMutableSet *clonedSet = [duplicated mutableSetValueForKey:keyName];
        NSEnumerator *e = [sourceSet objectEnumerator];
        NSManagedObject *relatedObject;
        while (relatedObject = [e nextObject]) {
            //Clone it, and add clone to set
            NSManagedObject *clonedRelatedObject = [self duplicateObject:relatedObject
                                                               temporary:temporary];
            [clonedSet addObject:clonedRelatedObject];
        }
    }
    
    return duplicated;
}


#pragma mark - Fetching

- (NSArray *)objectsWithEntity:(NSString *)entityName sortBy:(NSArray *)sortDescriptors predicateFormat:(NSString *)format, ...
{
    
    NSPredicate *predicate = nil;
    
    if (format) {
        va_list varArg;
        va_start(varArg, format);
        predicate = [NSPredicate predicateWithFormat:format
                                           arguments:varArg];
        va_end(varArg);
    }
    
    return [self objectsWithEntity:entityName
                            sortBy:sortDescriptors
                         predicate:predicate
                        fetchLimit:0
                       fetchOffset:0];
}

- (id)objectWithEntity:(NSString *)entityName sortBy:(NSArray *)sortDescriptors predicateFormat:(NSString *)format, ...
{
    
    NSPredicate *predicate = nil;
    
    if (format) {
        va_list varArg;
        va_start(varArg, format);
        predicate = [NSPredicate predicateWithFormat:format
                                           arguments:varArg];
        va_end(varArg);
    }
    
    NSArray *results = [self objectsWithEntity:entityName
                                        sortBy:sortDescriptors
                                     predicate:predicate
                                    fetchLimit:1
                                   fetchOffset:0];
    return results.count ? results.firstObject : nil;
}

- (NSArray *)objectsWithEntity:(NSString *)entityName sortBy:(NSArray *)sortDescriptors predicate:(NSPredicate *)predicate fetchLimit:(NSUInteger)limit fetchOffset:(NSUInteger)offset
{
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    
    if (sortDescriptors.count) {
        request.sortDescriptors = sortDescriptors;
    }
    
    request.predicate = predicate;
    
    if (limit) {
        request.fetchLimit = limit;
        if (offset)
            request.fetchOffset = offset;
    }
    
    NSError *error = nil;
    NSArray *result = [self.context executeFetchRequest:request
                                                  error:&error];
    if (!result)
        [NSException raise:NSGenericException format:@"%@", [error description]];
    
    return result;
}


@end
