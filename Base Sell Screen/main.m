//
//  main.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 24/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
