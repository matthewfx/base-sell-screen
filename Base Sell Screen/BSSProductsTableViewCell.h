//
//  BSSProductsTableViewCell.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSSTableViewCell.h"
#include "LineItem+Addition.h"

@protocol BSSProductTableViewCellDelegate <NSObject>

- (void)refreshSummary;

@end

@interface BSSProductsTableViewCell : BSSTableViewCell

@property (nonatomic, strong) LineItem *lineItem;
@property (nonatomic, weak) id <BSSProductTableViewCellDelegate> delegate;

@end
