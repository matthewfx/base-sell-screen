//
//  LineItem+Addition.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "LineItem.h"

@interface LineItem (Addition)

+ (LineItem *)lineItemWithParams:(NSDictionary *)params;
- (void)updateLineItemProductQuantityWithUserText:(NSString *)userText;
- (void)updateLineItemProductPriceWithUserText:(NSString *)userText;

@end
