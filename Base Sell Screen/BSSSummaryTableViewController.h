//
//  BSSSummaryTableViewController.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSSTableViewController.h"
#import "BSSSummary.h"


@interface BSSSummaryTableViewController : BSSTableViewController

@property (nonatomic, strong) NSString *percentageDiscountString;

- (void)refreshTable;

@end
