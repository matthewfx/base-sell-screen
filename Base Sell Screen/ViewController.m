//
//  ViewController.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 24/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "ViewController.h"
#import "Masonry.h"
#import "BSSProductTableViewController.h"
#import "BSSSummaryTableViewController.h"
#import "BSSCoreDataHelper.h"
#import "LineItem+Addition.h"
#import "UIAlertView+Blocks.h"

@interface ViewController ()

@property (nonatomic, strong) UIButton *addButton;
@property (nonatomic, strong) UIButton *boomshakalakaButton;
@property (nonatomic, strong) UIButton *discountButton;
@property (nonatomic, strong) BSSProductTableViewController *productsTVC;
@property (nonatomic, strong) BSSSummaryTableViewController *summaryTVC;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupButtons];
    [self setupTableViews];
}

- (void)setupButtons
{
    // background color
    self.view.backgroundColor = UIColorFromRGB(0xFAFAFA);
    
    // add button
    self.addButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.addButton.backgroundColor = UIColorFromRGB(0x3BAE45);
    [self.view addSubview:self.addButton];
    [self.addButton setTintColor:[UIColor whiteColor]];
    self.addButton.titleLabel.font = [UIFont fontWithName:@"Lato-Light" size:16.0];
    
    [self.addButton setTitle:@"Add Products"
                    forState:UIControlStateNormal];
    
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@30);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
        make.right.equalTo(@-20);
    }];
    
    [self.addButton addTarget:self
                       action:@selector(addProductsAction)
             forControlEvents:UIControlEventTouchUpInside];
    
    // add a discount
    
    self.discountButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.discountButton.backgroundColor = UIColorFromRGB(0x8B8B8B);
    [self.view addSubview:self.discountButton];
    [self.discountButton setTintColor:[UIColor whiteColor]];
    self.discountButton.titleLabel.font = [UIFont fontWithName:@"Lato-Light" size:16.0];
    
    [self.discountButton setTitle:@"Add a discount"
                    forState:UIControlStateNormal];
    
    
    [self.discountButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addButton.mas_bottom).offset(10.0);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
        make.right.equalTo(@-20);
    }];
    
    [self.discountButton addTarget:self
                       action:@selector(discountAction)
             forControlEvents:UIControlEventTouchUpInside];
    
    // boomshakalaka button
    
    self.boomshakalakaButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.boomshakalakaButton.backgroundColor = UIColorFromRGB(0x3BAE45);
    self.boomshakalakaButton.layer.cornerRadius = 3.0;
    [self.view addSubview:self.boomshakalakaButton];
    [self.boomshakalakaButton setTintColor:[UIColor whiteColor]];
    self.boomshakalakaButton.titleLabel.font = [UIFont fontWithName:@"Lato-Light" size:16.0];
    
    [self.boomshakalakaButton setTitle:@"Boomshakalaka"
                              forState:UIControlStateNormal];
    
    [self.boomshakalakaButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@-20);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
        make.right.equalTo(@-20);
    }];
    
    [self.boomshakalakaButton addTarget:self
                       action:@selector(boomshakalakaAction)
             forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupTableViews
{
    // products table view
    self.summaryTVC = [[BSSSummaryTableViewController alloc] init];
    
    [self addChildViewController:self.summaryTVC];
    [self.view addSubview:self.summaryTVC.view];
    
    [self.summaryTVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view);
        make.bottom.equalTo(self.boomshakalakaButton.mas_top).with.offset(-10.0);
        make.height.equalTo(@120);
    }];
    
    [self.summaryTVC didMoveToParentViewController:self];

    
    // summary table view
    
    self.productsTVC = [[BSSProductTableViewController alloc] init];
    
    [self addChildViewController:self.productsTVC];
    [self.view addSubview:self.productsTVC.view];
    
    [self.productsTVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view);
        make.top.equalTo(self.discountButton.mas_bottom).with.offset(10.0);
        make.bottom.equalTo(self.summaryTVC.view.mas_top).with.offset(-10.0);
    }];
    
    [self.productsTVC didMoveToParentViewController:self];
}


- (void)addProductsAction
{
    NSDictionary *params = @{@"quantity"    : @(1),
                             @"name"        : @"Eternal Teeshirt",
                             @"price"       : @(260.86956),
                             @"tax"         : @(0.15),
                             @"date"        : [NSDate date]};

    [LineItem lineItemWithParams:params];
    [self.summaryTVC refreshTable];
}

- (void)discountAction
{
    [UIAlertView showAlertViewWithTitle:@"Add discount for transaction"
                                message:@"Enter discount in %"
                           cancelButton:@"Cancel"
                       otherButtonTitle:@"OK"
                              onDismiss:^(NSString *userText) {
                                  self.summaryTVC.percentageDiscountString = userText;
                              } onCancel:^{
                                  
                              }];
}

- (void)boomshakalakaAction
{
    [[BSSCoreDataHelper sharedInstance] deleteAllObjectsForEntityName:@"LineItem"];
    self.summaryTVC.percentageDiscountString = @"0";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
