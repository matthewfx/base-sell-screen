//
//  AppDelegate.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 24/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

