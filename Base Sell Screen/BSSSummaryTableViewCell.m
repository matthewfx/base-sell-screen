//
//  BSSSummaryTableViewCell.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSSummaryTableViewCell.h"
#import "Masonry.h"

@implementation BSSSummaryTableViewCell


- (void)setupLabelsAndButtons
{
    [super setupLabelsAndButtons];
    
    // arrange labels for the cell
    UIFont *cellFont = [UIFont fontWithName:@"Lato-Regular" size:14.0];
    UIColor *cellTextColor = UIColorFromRGB(0x3E3E3E);
    
    self.cellNameLabel = [[UILabel alloc] init];
    self.cellNameLabel.textColor = cellTextColor;
    self.cellNameLabel.textAlignment = NSTextAlignmentLeft;
    self.cellNameLabel.font = cellFont;
    [self addSubview:self.cellNameLabel];
    
    [self.cellNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(@20);
        make.height.equalTo(@20);
        make.width.equalTo(@150);
        
    }];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.textColor = cellTextColor;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    self.priceLabel.font = cellFont;
    [self addSubview:self.priceLabel];
    
    
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(@-20);
        make.height.equalTo(@20);
        make.width.equalTo(@120);
    }];
    
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
