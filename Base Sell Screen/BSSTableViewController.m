//
//  BSSTableViewController.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSTableViewController.h"
#import "Masonry.h"

@interface BSSTableViewController ()

@end

@implementation BSSTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // add top separator
    self.view.backgroundColor = [UIColor clearColor];
    UIView *separatorView = [UIView new];
    separatorView.backgroundColor = UIColorFromRGB(0xEFEFEF);
    [self.view addSubview:separatorView];
    
    [separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).with.offset(0.0);
        make.height.equalTo(@2);
        make.width.equalTo(self.view);
        
    }];

    
    [self setupTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupTableView
{
    // configure table view
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = [UIView new];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    // override
}

- (void)refreshTable
{
    [self.tableView reloadData];
}

@end
