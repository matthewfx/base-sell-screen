//
//  UIAlertView+Blocks.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 26/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "UIAlertView+Blocks.h"

static CompletionBlockWithUserEnteredText _completionBlock;
static CancelBlock _cancelBlock;

@implementation UIAlertView (Blocks)

+ (UIAlertView *)showAlertViewWithTitle:(NSString *)title
                                message:(NSString *)message
                           cancelButton:(NSString *)cancelButtonTitle
                       otherButtonTitle:(NSString *)otherButtonTitle
                              onDismiss:(CompletionBlockWithUserEnteredText)completed
                               onCancel:(CancelBlock)cancelled
{
    _cancelBlock = [cancelled copy];
    _completionBlock = [completed copy];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message delegate:[self self]
                                          cancelButtonTitle:cancelButtonTitle
                                          otherButtonTitles:nil];
    
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    UITextField *textField = [alert textFieldAtIndex:0];
    [textField setKeyboardType:UIKeyboardTypeDecimalPad];
    
    [alert addButtonWithTitle:otherButtonTitle];
    
    
    [alert show];
    
    return alert;
}

+ (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == [alertView cancelButtonIndex]) {
        _cancelBlock();
    } else {
        UITextField *userText = [alertView textFieldAtIndex:0];
        _completionBlock(userText.text);
    }
}


@end
