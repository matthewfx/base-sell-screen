//
//  BSSTableViewCell.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Masonry.h"

@interface BSSTableViewCell : UITableViewCell

- (void)setupLabelsAndButtons;


@end
