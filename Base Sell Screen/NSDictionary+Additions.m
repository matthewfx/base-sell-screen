//
//  NSDictionary+Additions.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (id)safeObjectForKey:(NSString *)key {
    id object = [self objectForKey:key];
    
    if ([object isKindOfClass:[NSNull class]])
        return nil;
    
    return object;
}

- (id)safeObjectForKeyPath:(NSString *)keyPath {
    id object = [self valueForKeyPath:keyPath];
    
    if ([object isKindOfClass:[NSNull class]])
        return nil;
    
    return object;
}

@end
