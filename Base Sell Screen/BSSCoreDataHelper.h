//
//  BSSCoreDataHelper.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface BSSCoreDataHelper : NSObject

@property (nonatomic, readonly) NSManagedObjectContext *context;
@property (nonatomic, readonly) NSManagedObjectContext *parentContext;
@property (nonatomic, readonly) NSManagedObjectModel *model;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, readonly) NSPersistentStore *store;

+ (id)sharedInstance;

- (void)setupCoreData;
- (void)saveContext;
- (void)backgroundSaveContext;
- (void)clearCoreData;
- (void)deleteAllObjectsForEntityName:(NSString *)entity;

- (id)createObjectWithEntity:(NSString *)entity;
- (id)temporaryObjectWithEntity:(NSString *)entity;
- (void)deleteObject:(NSManagedObject *)object;
- (id)duplicateObject:(NSManagedObject *)source
            temporary:(BOOL)temporary;

- (NSArray *)objectsWithEntity:(NSString *)entityName
                        sortBy:(NSArray *)sortDescriptors
               predicateFormat:(NSString *)format, ...;

- (id)objectWithEntity:(NSString *)entityName
                sortBy:(NSArray *)sortDescriptors
       predicateFormat:(NSString *)format, ...;

- (NSArray *)objectsWithEntity:(NSString *)entityName
                        sortBy:(NSArray *)sortDescriptors
                     predicate:(NSPredicate *)predicate
                    fetchLimit:(NSUInteger)limit
                   fetchOffset:(NSUInteger)offset;

@end
