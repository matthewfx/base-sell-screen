//
//  BSSProductTableViewController.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSSTableViewController.h"

@interface BSSProductTableViewController : BSSTableViewController

@end
