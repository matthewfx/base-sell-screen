//
//  BSSTableViewCell.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSTableViewCell.h"

@interface BSSTableViewCell ()

@end

@implementation BSSTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupLabelsAndButtons];
        [self setupSeparators];
    }
    
    return self;
}

- (void)setupLabelsAndButtons
{
   // override
}

- (void)setupSeparators
{
    // add separator at the bottom of the cell
    UIView *separatorView = [UIView new];
    separatorView.backgroundColor = UIColorFromRGB(0xEFEFEF);
    [self addSubview:separatorView];
    
    [separatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom).with.offset(0.0);
        make.height.equalTo(@2);
        make.width.equalTo(self);
        
    }];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
