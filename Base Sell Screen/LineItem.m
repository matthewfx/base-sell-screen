//
//  LineItem.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "LineItem.h"


@implementation LineItem

@dynamic productName;
@dynamic productQuantity;
@dynamic productPrice;
@dynamic productTaxRate;
@dynamic productAdded;

@end
