//
//  BSSProductsTableViewCell.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "BSSProductsTableViewCell.h"
#import "UIAlertView+Blocks.h"

@interface BSSProductsTableViewCell ()

@property (nonatomic, strong) UIButton *productQuantityButton;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UIButton *productPriceButton;
@property (nonatomic, strong) UILabel *xLabel;

@end

@implementation BSSProductsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setupLabelsAndButtons
{
    [super setupLabelsAndButtons];
    
    // arrange labels  and buttons for the cell
    
    UIFont *cellFont = [UIFont fontWithName:@"Lato-Regular" size:16.0];
    UIColor *cellTextColor = UIColorFromRGB(0x3E3E3E);
    UIColor *buttonColor = UIColorFromRGB(0x3BAE45);
    UIFont *buttonFont = [UIFont fontWithName:@"Lato-Bold" size:16.0];
    
    self.productQuantityButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.productQuantityButton.backgroundColor = buttonColor;
    self.productQuantityButton.titleLabel.font = buttonFont;
    [self.productQuantityButton setTintColor:[UIColor whiteColor]];
    [self.productQuantityButton addTarget:self
                                   action:@selector(quantityButtonAction)
                         forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.productQuantityButton];
    
    [self.productQuantityButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(@20);
        make.height.equalTo(@25);
        make.width.equalTo(@40);
        
    }];

    self.xLabel = [[UILabel alloc] init];
    self.xLabel.textColor = UIColorFromRGB(0x8B8B8B);
    self.xLabel.textAlignment = NSTextAlignmentCenter;
    self.xLabel.font = cellFont;
    self.xLabel.text = @"x";
    [self addSubview:self.xLabel];
    
    
    [self.xLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.productQuantityButton.mas_right).with.offset(10.0);
        make.height.equalTo(@20);
        make.width.equalTo(@10);
    }];
    
    self.productNameLabel = [[UILabel alloc] init];
    self.productNameLabel.textColor = cellTextColor;
    self.productNameLabel.textAlignment = NSTextAlignmentLeft;
    self.productNameLabel.font = cellFont;
    [self addSubview:self.productNameLabel];
    
    
    [self.productNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.xLabel.mas_right).with.offset(10.0);
        make.height.equalTo(@20);
        make.width.equalTo(@130);
    }];
    
    self.productPriceButton= [UIButton buttonWithType:UIButtonTypeSystem];
    self.productPriceButton.backgroundColor = buttonColor;
    self.productPriceButton.titleLabel.font = buttonFont;
    [self.productPriceButton setTintColor:[UIColor whiteColor]];
    [self.productPriceButton addTarget:self
                                action:@selector(priceButtonAction)
                      forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:self.productPriceButton];
    
    
    [self.productPriceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(@-20);
        make.height.equalTo(@25);
        make.width.equalTo(@75);
    }];

}

- (void)setLineItem:(LineItem *)lineItem
{
    _lineItem = lineItem;
    [self.productQuantityButton setTitle:[NSString stringWithFormat:@"%ld",(long)[lineItem.productQuantity integerValue]]
                                forState:UIControlStateNormal];
    self.productNameLabel.text = self.lineItem.productName;
    double price = [self.lineItem.productPrice doubleValue] + [self.lineItem.productPrice doubleValue] * [self.lineItem.productTaxRate doubleValue];
    [self.productPriceButton setTitle:[NSString stringWithFormat:@"%.2f",price]
                                forState:UIControlStateNormal];
}

- (void)quantityButtonAction
{
    [UIAlertView showAlertViewWithTitle:@"Change product quantity"
                                message:@"Enter new product quantity"
                           cancelButton:@"Cancel"
                       otherButtonTitle:@"OK"
                              onDismiss:^(NSString *userText) {
                                  [self.lineItem updateLineItemProductQuantityWithUserText:userText];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"ProductChanged"
                                                                                      object:nil];
                              } onCancel:^{
                                  
                              }];
}

- (void)priceButtonAction
{
    [UIAlertView showAlertViewWithTitle:@"Change product price"
                                message:@"Enter new product exclusive price"
                           cancelButton:@"Cancel"
                       otherButtonTitle:@"OK"
                              onDismiss:^(NSString *userText) {
                                  [self.lineItem updateLineItemProductPriceWithUserText:userText];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:@"ProductChanged"
                                                                                      object:nil];
                              } onCancel:^{
                                  
                              }];

}


@end
