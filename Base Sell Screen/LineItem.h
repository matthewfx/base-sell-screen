//
//  LineItem.h
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 25/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LineItem : NSManagedObject

@property (nonatomic, retain) NSString * productName;
@property (nonatomic, retain) NSNumber * productQuantity;
@property (nonatomic, retain) NSNumber * productPrice;
@property (nonatomic, retain) NSNumber * productTaxRate;
@property (nonatomic, retain) NSDate   * productAdded;

@end
