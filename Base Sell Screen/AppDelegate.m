//
//  AppDelegate.m
//  Base Sell Screen
//
//  Created by Mateusz Nuckowski on 24/05/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "AppDelegate.h"
#import "BSSCoreDataHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[BSSCoreDataHelper sharedInstance] setupCoreData];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[BSSCoreDataHelper sharedInstance] backgroundSaveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
     [[BSSCoreDataHelper sharedInstance] backgroundSaveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
     [[BSSCoreDataHelper sharedInstance] backgroundSaveContext];
}

@end
